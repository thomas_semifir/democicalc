package fr.semifir.springcalctest;

public class Calculatrice {

    public Integer additionner(Integer nb1, Integer nb2) {
        return nb1 + nb2;
    }

    public Integer multiplier(Integer nb1, Integer nb2){
        return nb1 * nb2;
    }

    public Integer diviser(Integer nb1, Integer nb2){
        return nb1/nb2;
    }

    public Integer soustraire(Integer nb1, Integer nb2) {
        return nb1 - nb2;
    }

    public String manger(){
        return "Mouahahahah";
    }
}
